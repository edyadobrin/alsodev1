<?php

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user.news.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'  => 'required',
            'email' => 'required',
            'text'  => 'required',
        ]);

        $request['homepage'] = isset( $request['homepage']) ? $request['homepage'] : " ";

        if (strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome'))
        {
            $browserInfo = 'Chrome';
        }
        elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox'))
        {
            $browserInfo = 'Firefox';
        }
        else
        {
            $browserInfo = 'Default';
        }
        $IP = $_SERVER['REMOTE_ADDR'];

        News::create([
            'userName'      => $request['name'],
            'email'         => $request['email'],
            'homepage'      => $request['homepage'],
            'text'          => $request['text'],
            'browserInfo'   => $browserInfo,
            'IP'            => $IP,
        ]);

        return redirect()->route('news.index');
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        //
    }

    public function getNews(Request $request, News $news)
    {
        $data = $news->getData();

        return \DataTables::of($data)
            ->addColumn('Created', function($data)
            {
                return  date($data->created_at);
            })
            ->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        //
    }
}
