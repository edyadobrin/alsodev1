<?php

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('articles.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('articles.create');
    }

    /**
     * Get the data for listing in yajra.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getArticles(Request $request, News $news)
    {
        $data = $news->getData();

        return \DataTables::of($data)
            ->addColumn('Actions', function($data) {
                    return '<button type="button" class="btn btn-primary btn-sm" id="getEditNewsData" data-id="'.$data->id.'"><i class="fas fa-edit"></i></button>
                    <button type="button" data-id="'.$data->id.'" data-toggle="modal" data-target="#DeleteNewsModal" class="btn btn-danger btn-sm" id="getDeleteId"><i class="fas fa-trash-alt"></i></button>';

            })
            ->rawColumns(['Actions'])

            ->addColumn('Created', function($data)
            {
                return  date($data->created_at);
            })

            ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'  => 'required',
            'email' => 'required',
            'text'  => 'required',
        ]);

        $request['homepage'] = isset( $request['homepage']) ? $request['homepage'] : " ";

        if (strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome'))
        {
            $browserInfo = 'Chrome';
        }
        elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox'))
        {
            $browserInfo = 'Firefox';
        }
        else
        {
            $browserInfo = 'Default';
        }
        $IP = $_SERVER['REMOTE_ADDR'];

        News::create([
            'userName'      => $request['name'],
            'email'         => $request['email'],
            'homepage'      => $request['homepage'],
            'text'          => $request['text'],
            'browserInfo'   => $browserInfo,
            'IP'            => $IP,
        ]);

        return redirect()->route('articles.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news, $id)
    {
        $news = News::find($id);
        return view('articles.edit',compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'  => 'required',
            'email' => 'required',
            'text'  => 'required',
        ]);

        $news = News::find($id);
        $news->update([
            'userName'      => $request['name'],
            'email'         => $request['email'],
            'text'          => $request['text'],
        ]);

        return redirect()->route('articles.index')
            ->with('success','News updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\News  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = new News;
        $news->find($id)->delete();

        return response()->json(['success'=>'News deleted successfully']);
    }
}
