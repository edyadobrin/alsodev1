<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\NewsController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/admin','App\Http\Controllers\AdminController@index');

Auth::routes(['register' => false]);
//Auth::routes();

Route::resource('news', NewsController::class);

Route::resource('articles', ArticleController::class);
// Route for get articles for yajra post request.
Route::get('get-articles', [ArticleController::class, 'getArticles'])->name('get-articles');
Route::get('get-news', [NewsController::class, 'getNews'])->name('get-news');
