@extends('adminlte::page')

@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <h2 class="card-header bg-secondary text-white">Edit News</h2>
                <div class="card-body">    
                    <div class="row">
                        <div class="col-lg-12 margin-tb">
                            <div class="pull-right">
                                <a class="btn btn-primary" href="{{ route('articles.index') }}"> Back</a>
                            </div>
                        </div>
                    </div>
                    <form action="{{ route('articles.update',$news->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                         <div class="row">
                            {{-- Name field --}}
                            <div class="col-xs-12 col-sm-12 col-md-12"> 
                                <strong>Name:</strong>
                                <input type="text" name="name" maxlength="250" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
                                       value="{{ old('name') == "" ? $news->userName : old('name') }}" placeholder="Name" autofocus>
                                @if($errors->has('name'))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </div>
                                @endif
                            </div> 
                            {{-- Email field --}}
                             <div class="col-xs-12 col-sm-12 col-md-12"> 
                                <strong>Email:</strong>
                                <input type="email" name="email" maxlength="250" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}"
                                       value="{{ old('email') == "" ? $news->email : old('email') }}" placeholder="Email">
                                @if($errors->has('email'))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </div>
                                @endif
                            </div>

                            {{-- text field --}}
                            <div class="col-xs-12 col-sm-12 col-md-12"> 
                                <strong>Text:</strong>
                                <textarea type="text" name="text" rows="5" maxlength="250" class="form-control {{ $errors->has('text') ? 'is-invalid' : '' }}"
                                    placeholder="Text">{{ old('text') == "" ? $news->text : old('text') }}</textarea>
                                @if($errors->has('text'))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->first('text') }}</strong>
                                    </div>
                                @endif
                            </div>

                       </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                              <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>    
@endsection

