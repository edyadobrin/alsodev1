@extends('layouts.app')

@section('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card" style="margin-top: 20px;">
                    <h2 class="card-header bg-secondary text-white">Add New News</h2>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12 margin-tb">
                                <div class="pull-right">
                                    <a class="btn btn-primary" href="{{ route('news.index') }}"> Back</a>
                                </div>
                            </div>
                        </div>
                        <form action="{{ route('news.store') }}" method="POST">
                            @csrf
                            <div class="row" style="padding-left:8px;padding-right:8px;"  } >
                                {{-- Name field --}}
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <strong>Name:</strong>
                                    <input type="text" name="name" maxlength="250" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
                                           value="{{ old('name') }}" placeholder="Name" autofocus>
                                    @if($errors->has('name'))
                                        <div class="invalid-feedback">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </div>
                                    @endif
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <strong>Email:</strong>
                                    <input type="email" name="email" maxlength="250" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}"
                                           value="{{ old('email') }}" placeholder="Email" autofocus>
                                    @if($errors->has('email'))
                                        <div class="invalid-feedback">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </div>
                                    @endif
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <strong>Home Page:</strong>
                                    <input type="text" name="homePage" maxlength="250" class="form-control {{ $errors->has('homePage') ? 'is-invalid' : '' }}"
                                           value="{{ old('homePage') }}" placeholder="Home Page" autofocus>
                                    @if($errors->has('homePage'))
                                        <div class="invalid-feedback">
                                            <strong>{{ $errors->first('homePage') }}</strong>
                                        </div>
                                    @endif
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <strong>Text:</strong>
                                    <textarea name="text" id="exampleFormControlTextarea" rows="5" class="form-control {{ $errors->has('text') ? 'is-invalid' : '' }}"
                                              value="{{ old('text') }}" placeholder="Text" autofocus></textarea>
                                    @if($errors->has('text'))
                                        <div class="invalid-feedback">
                                            <strong>{{ $errors->first('text') }}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="row" style="padding-left:8px;padding-right:8px;margin-top: 20px;"  } >
                                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
