@extends('layouts.app')

@section('content')
    <html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    </head>
    <body>
    <div class="container">
        <div class="justify-content-center align-items-center">
            <form>
                <div class="form-group">
                    <label for="exampleFormControlInput">User name</label>
                    <input type="text" class="form-control" id="exampleFormControlInput" placeholder="Username">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput">Email address</label>
                    <input type="email" class="form-control" id="exampleFormControlInput" placeholder="name@example.com">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput">Homepage</label>
                    <input type="url" class="form-control" id="exampleFormControlInput" placeholder="https://example.com">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea">Text</label>
                    <textarea class="form-control" id="exampleFormControlTextarea" rows="5"></textarea>
                </div>
                <div class="form-group">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Send</button>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">News</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </body>
    </html>
@endsection
